autocmd! bufwritepost .vimrc source %

filetype plugin on
syntax on			"show syntax

set relativenumber		"show numbers as relative
set number
set tabstop=4			"4 spaces as tab
set shiftwidth=4		"4 spaces on <<>>
set softtabstop=4		"actual tab press
set expandtab			"make tabs spaces
set splitright          "split right then below 
set splitbelow          " ^ ^ 
set lazyredraw          " fix screen tearing 
set backspace=indent,eol,start "Made backspace nice
"List chars for nice marking

set list
set listchars=tab:▸\ ,eol:¬,extends:❯,precedes:❮
set laststatus=2 "always show status bar
"Omnicomplete
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone,preview
"searching
set ignorecase			"ignore word case when searching
set incsearch			"incremenet search
set showmatch			"show matches
set hlsearch			"hl search
set wrap			"wrap text


"remap ; to :
nnoremap ; :
nnoremap : ;

" " Switch tabs with ctrl + h/l
nnoremap <silent><S-n> :tabnew<CR>
nnoremap <silent><S-h> :tabp<CR>
nnoremap <silent><S-l> :tabn<CR>

"colorscheme zenburn
colorscheme grb256

call plug#begin('~/.vim/plugged')                       " Plugin manager

Plug 'vim-airline/vim-airline'                          " Airline status bar
Plug 'vim-airline/vim-airline-themes'                   " More themes
Plug 'tpope/vim-surround'                               " Surround is the only motion missing
Plug 'tpope/vim-commentary'                             " Commenting
Plug 'Townk/vim-autoclose'                              " auto close brackets etc.
Plug 'ctrlpvim/ctrlp.vim'                               " fuzzy finder

call plug#end() " End pluggin


let g:neocomplete#enable_at_startup = 1                 " Use neocomplete ate startup
let g:neocomplete#enable_smart_case = 1                 " use smart case


inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal autoindent syntax=html omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=jedi#completions
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType python setlocal completeopt=preview   " No docstring

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
